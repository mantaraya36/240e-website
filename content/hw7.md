# 240E Final Project Proposal

Aaron: Music information retrieval techniques applied to algorithmic composition

Anis: Automatic detection of licks in an improvised passage

Hafiz: Automatic segmentation of North Indian classical music/Time on pitch histogram (Krumhansl 1990) of non western music

Owen: Polyphonic note detection and tracking

Rob: Shifty Looping-x: Expressive, Meter-aware, non-repeating rhythmic looping

Sean: Research and implement a system that uses low level features to describe sound effects
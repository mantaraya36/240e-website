Title: Week 3 - Audio Feature Extraction I
Date: 24-01-2014

Notes:

* [Audio Features](|filename|/notes/Audio_features.html)

Ipython notebooks:

* [Audio Features I](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Audio Features I.ipynb)

Homework
--------
*due: Friday January 31th*

On your music collection perform the measurement of RMS and plot the result as a histogram. Separate your collection by artist, album or genre, and show any differences or similarities in the values of RMS.

Additionally, select 5 tracks from your collection and extract windowed RMS. Explore the effect of different sized windows on the result.

Reading:
--------
*due: Friday January 31th*

G. Tzanetakis. Chapter 2: Audio Feature Extraction. Sections 2.1 and 2.2. Pages 43-57. From Music Data Mining.


Additional Reading:
-------------------
Peeters, G. (2004). A large set of audio features for sound description (similarity and classification) in the CUIDADO project (pp. 1–25). Retrieved from http://www.citeulike.org/group/1854/article/1562527

[MPEG-7 Audio](http://mpeg.chiariglione.org/standards/mpeg-7/audio)

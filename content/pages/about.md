Title: Info
Date: 2014-01-01
Author: Andres Cabrera
Category: Information

*Lecturer:* Andrés Cabrera <andres@mat.ucsb.edu>

*Room:* Music 2215

*TA:* Yuan-yi Fan <yyf.plus@gmail.com> 

*Quarter:* Winter 2014

*Mailing list:* [http://lists.create.ucsb.edu/mailman/listinfo/240](http://lists.create.ucsb.edu/mailman/listinfo/240)

Summary
===========================================

Music is a complex and multi-layered conveyor of information and emotion. Music Information Retrieval seeks to extract meaning and elements from music to enable better classification, query, matching, recommendation and transformation of musical material and sets. This course hopes to serve as a hands on introduction into the practice of Music Information Retrieval and Music Data Mining.

[Syllabus](|filename|/res/240E-course-outline.pdf)

These are the websites for previous courses for the 240 series:

* [240A](http://mat.ucsb.edu/240/A)
* [240B](http://mat.ucsb.edu/240/B)
* [240C](http://mat.ucsb.edu/240/C)
* [240D](http://mat.ucsb.edu/240/D)





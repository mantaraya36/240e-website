Title: Week 4 - Audio Feature Extraction II
Date: 27-01-2014

Ipython notebooks:

* [Audio Features II-Temporal and Spectral](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Audio Features II-Temporal and Spectral.ipynb?create=1)

Homework 4
----------
*due: Friday February 7th*

For at least 5 pieces in your collection (try to choose some that are very different, but include some similar ones too), extract 6 temporal or spectral features. Analyze and discuss what the features could tell us about the recordings. Explore how different window sizes and smoothing might affect the results.

Reading
-------
*due: Friday February 7th*

Lerch, A. (n.d.). Chapter 5: Tonal analysis. In An Introduction to Audio Content Analysis (pp. 79–103). Up to the Polyphonic Input signal chapter.

Further Reading
---------------

Rabiner, L., Cheng, M., Rosenberg, a., & McGonegal, C. (1976). A comparative performance study of several pitch detection algorithms. IEEE Transactions on Acoustics, Speech, and Signal Processing, 24(5), 399–418. doi:10.1109/TASSP.1976.1162846

Klapuri, A., & Davy, M. (2006). Signal Processing Methods for Music Transcription. Chapter 4. Beat tracking and music metre analysis.
Title: Homework 1 and 2 submissions
Date: 28-01-2014


Homework 1
========== 

* Aaron: [Aaron_hw1](|filename|/students/Aaron_hw1.html)
* Anis: [http://nbviewer.ipython.org/gist/anisharon/8443429](http://nbviewer.ipython.org/gist/anisharon/8443429)
* Hafiz: [Hafiz_hw1.zip](|filename|/students/Hafiz_hw1.zip)
* Joseph: [http://nbviewer.ipython.org/gist/anonymous/8470255](http://nbviewer.ipython.org/gist/anonymous/8470255)
* Owen: [http://nbviewer.ipython.org/gist/owengc/8486332](http://nbviewer.ipython.org/gist/owengc/8486332) , [http://nbviewer.ipython.org/gist/owengc/8486246](http://nbviewer.ipython.org/gist/owengc/8486246)
* Rob:  [https://gist.github.com/milrob/8483898](https://gist.github.com/milrob/8483898)
* Sean: [http://nbviewer.ipython.org/gist/soundbysean/8487190](http://nbviewer.ipython.org/gist/soundbysean/8487190)


Homework 2
==========

* Aaron: [Aaron_hw2](|filename|/students/Aaron_hw2.html)
* Anis: [http://nbviewer.ipython.org/gist/anisharon/8593377](http://nbviewer.ipython.org/gist/anisharon/8593377)
* Hafiz: [http://nbviewer.ipython.org/gist/muhammadhafiz/2342ea25562384e57037](http://nbviewer.ipython.org/gist/muhammadhafiz/2342ea25562384e57037)
* Owen: [http://nbviewer.ipython.org/gist/owengc/75068e105523d0773e33](http://nbviewer.ipython.org/gist/owengc/75068e105523d0773e33)
* Rob: [https://gist.github.com/milrob/16fa49709529722f5e6e](https://gist.github.com/milrob/16fa49709529722f5e6e)
* Sean: [http://nbviewer.ipython.org/github/soundbysean/240E_iPython/blob/master/240E_HW2_SCP.ipynb?create=1](http://nbviewer.ipython.org/github/soundbysean/240E_iPython/blob/master/240E_HW2_SCP.ipynb?create=1)

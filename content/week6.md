Title: Week 6 - Genre and Mood detection
Date: 10-02-2014

Notes:

* [Genre](|filename|/notes/Genre.html) 
* [Mood](|filename|/notes/Mood.html) 

Ipython notebooks:

* [Feature summarization](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Feature%20summary%20and%20analysis.ipynb)
* [Marsyas](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Marsyas.ipynb?create=1)

Homework 6
----------
*due: Friday 21th February*

Explore summarization of features extracted in HW5 (if it makes sense) either through "Texture windows", Self-similarity matrices, or other techinques.

Reading
-------
*due: Friday 21th February*

Tzanetakis, G., & Cook, P. (2002). Musical genre classification of audio signals. IEEE Transactions on Speech and Audio Processing, 10(5), 293–302. Retrieved from http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=1021072


Further Reading
---------------
Jain, A., Duin, R., & Mao, J. (2000). Statistical pattern recognition: A review. IEEE Transactions on Pattern Analysis and Machine Intelligence, 22(1), 4–37. Retrieved from http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=824819

Lerch, A. (n.d.). Chapter 8: MUSICAL GENRE, SIMILARITY, AND MOOD. In An Introduction to Audio Content Analysis.

Ogihara, M., & Kim, Y. (2012). Chapter 5:Mood and Emotional Classification. In Music Data Mining.

Trohidis, K., Tsoumakas, G., Kalliris, G., & Vlahavas, I. (2008). Multi-Label Classification of Music into Emotions. ISMIR, 325–330. Retrieved from http://books.google.com/books?hl=en&lr=&id=OHp3sRnZD-oC&oi=fnd&pg=PA325&dq=MULTI-LABEL+CLASSIFICATION+OF+MUSIC+INTO+EMOTIONS&ots=oDSLrDjwc2&sig=i5KPRtHqhp74L6BIVpHecgYp6bw
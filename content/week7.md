Title: Week 7 - Segmentation
Date: 21-02-2014

Notes:

* [Segmentation](|filename|/notes/Segmentation.html) 

Ipython notebooks:

* [Segmentation](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Segmentation.ipynb)

Homework 7
----------
*due: Friday 28th February*

Submit a written proposal for your final project detailing the goals and techniques used. Detail the deliverables you will submit as your project.

Reading
-------
*due: Friday 28th February*

Tzanetakis, G., & Cook, P. (1999). Multifeature audio segmentation for browsing and annotation. IEEE Workshop on Applications of Signal Processing to Audio and Acoustics, 1–4. Retrieved from http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=810860

Additional Reading
------------------

Foote, J. (2000). Automatic audio segmentation using a measure of audio novelty. Multimedia and Expo, 2000. ICME 2000. 2000 IEEE …, 1, 452–455. Retrieved from http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=869637

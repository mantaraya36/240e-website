title: Week 1 - Introduction
date: 06-01-2014

Class Notes:

* [Introduction to Scientific computing in Python](|filename|/notes/Intro.html)
* [Music Information Retrieval](|filename|/notes/MIR.html)

Ipython Notebooks:

* [Python Basics](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Python%20Basics.ipynb)
* [Audio File I/O](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Audio%20File%20IO.ipynb?create=1)

Homework 1
----------
*Due Friday January 17th*

Install the required tools in your system, and produce a histogram showing the length of audio files in your audio collection.

Reading
-------
*For Monday January 13th*

Li, T., & Li, L. (2012). Chapter 1: Music Data Mining : An Introduction. In *Music Data Mining* (pp. 3–42).

(This text is available online from the UCSB library when connected inside campus or from the campus VPN)
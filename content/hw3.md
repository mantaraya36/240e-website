Title: Homework 3 Submissions
Date: 03-02-2014

 Joseph: 
[http://nbviewer.ipython.org/gist/anonymous/8779651](http://nbviewer.ipython.org/gist/anonymous/8779651)

Sean: 
[http://nbviewer.ipython.org/github/soundbysean/240E_iPython/blob/master/240E_HW3_SCP.ipynb?create=1](http://nbviewer.ipython.org/github/soundbysean/240E_iPython/blob/master/240E_HW3_SCP.ipynb?create=1)

Owen: 
[http://nbviewer.ipython.org/gist/owengc/a02743bbae4e3f2a51b1](http://nbviewer.ipython.org/gist/owengc/a02743bbae4e3f2a51b1)
[http://nbviewer.ipython.org/gist/owengc/fa675b43cd9a62310733](http://nbviewer.ipython.org/gist/owengc/fa675b43cd9a62310733)

Aaron: 
[http://nbviewer.ipython.org/gist/aadjones/8727729](http://nbviewer.ipython.org/gist/aadjones/8727729)

Rob: 
[https://gist.github.com/milrob/3f4d06b950f9899406ee](https://gist.github.com/milrob/3f4d06b950f9899406ee)

Hafiz:
[http://nbviewer.ipython.org/gist/muhammadhafiz/c0692b3564b19e1a7442](http://nbviewer.ipython.org/gist/muhammadhafiz/c0692b3564b19e1a7442)
[http://nbviewer.ipython.org/gist/muhammadhafiz/7a33ebd65fade9c567e6](http://nbviewer.ipython.org/gist/muhammadhafiz/7a33ebd65fade9c567e6)

Anis: 
[http://nbviewer.ipython.org/gist/anisharon/8727035](http://nbviewer.ipython.org/gist/anisharon/8727035)
